var AWS = require('aws-sdk');
var DOC = require('dynamodb-doc');
var dynamo = new DOC.DynamoDB();

var networks = ["LinkedIn"];

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data);
};

var cb = function(err, data) {
    if(err) {
        console.log(err);
    } else {

    }
};

var messageOutput = "Results\n";

exports.handler = function(event, context) {
    var cb = function(err, data) {
        if(err) {
            console.log('error on RetrieveSocialFeed: ',err);
            context.done('Unable to retrieve Social Site Data', null);
        } else {
            data.Items.forEach(function(item) {
                if (item && item.SiteId) {
                    networks.forEach(function (elem) {
                        if (item[[elem] + "Status"]) {
                            loopThroughNetworks(elem, item[[elem] + "Id"], item.SiteId);
                        }
                    });
                }
            });
        }
    };

    var params = {
        TableName : "SocialSiteSettings",
        FilterExpression: "SiteStatus = :siteStatus",
        ExpressionAttributeValues: {
            ":siteStatus": true,
        }
    };

    dynamo.scan(params, cb);
};

function loopThroughNetworks(network, networkId, siteId) {
    // call the correct function for the network in question returning a count of items found
    eval([network] + "Feed")(networkId, siteId);
}

function LinkedInFeed(networkId, siteId) {

    var Linkedin = require('node-linkedin')('xxxx', 'xxxx', 'callback');

    Linkedin.companies.updates('xxxx', function(err, company) {
        // Gets all the updates(Posts) along with their details of a company
        console.log(company);
    });

}
